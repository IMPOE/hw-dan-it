const app = document.getElementById('app');
const grid = document.createElement('table');
app.appendChild(grid);
grid.className = 'wrap';
grid.id = 'grid';
let arrNum = [];
let arrCell = [];

class Field {
  constructor() {
    this.arrCell = arrCell;
    this.arrNum = arrNum;
  }

  bildField(number) {
    for (let i = 0; i < number; i++) {
      const cell = document.createElement('div');
      cell.className = 'cell';
      cell.classList.add('normal');
      this.arrCell.push(cell);
      this.arrNum.push(i);
      grid.appendChild(cell);
    }
  }
}

const newField = new Field();
newField.bildField(100);

class Cell {
  constructor() {
    this.arrNum = arrNum;
  }
  activeCell() {
    return this.arrNum.splice(
      Math.floor(Math.random() * this.arrNum.length),
      1
    );
  }
}

const cell = new Cell();

class Game {
  constructor(level = 'easy') {
    this.level = this.choseLevel();
    if (this.level === 'easy') {
      this.timeInterval = 1500;
    }
    if (this.level === 'medium') {
      this.timeInterval = 1000;
    }
    if (this.level === 'hard') {
      this.timeInterval = 500;
    }

    this.pointUser = 0;
    this.pointComputer = 0;
    this.processTimer = null;
  }

  choseLevel() {
    let enterLevel;
    console.log(this.name);
    while (enterLevel !== 1 && enterLevel !== 2 && enterLevel !== 3) {
      enterLevel = +prompt(
        'Let begin new game! Select the difficulty level: easy - 1, medium  - 2, hard - 3 '
      );
    }
    if (enterLevel === 1) return 'easy';
    if (enterLevel === 2) return 'medium';
    if (enterLevel === 3) return 'hard';
  }

  playerAction() {
    document.getElementById(grid);
    grid.addEventListener('click', function (event) {
      let target = event.target;
      if (target.classList.contains('normal')) {
        target.classList.add('active');
        const cell = target;
        clearTimeout(this.processTimer);
      }
      if (!target.classList.contains('cellCollor')) {
        target.classList.remove('active');
        return false;
      }
    });
  }

  whoWinner() {
    if (this.pointUser > this.pointComputer) {
      alert(
        `  YOUR POINTS "${this.pointUser}" - COMPUTER POINTS ${this.pointComputer}`
      );
      document.location.reload();
    }
    if (this.pointUser < this.pointComputer) {
      alert(
        ` YOUR POINTS "${this.pointUser}" - COMPUTER POINTS ${this.pointComputer}`
      );
      document.location.reload();
    }
  }
  startGame() {
    const goal = cell.activeCell();
    if (
      this.pointUser >= arrCell.length / 2 ||
      this.pointComputer >= arrCell.length / 2
    ) {
      return this.whoWinner();
    }
    if (arrCell[goal].classList.contains('cell')) {
      arrCell[goal].classList.add('cellCollor');
      this.playerAction();
      arrNum.slice(arrNum.indexOf(goal), 1);
      this.processTimer = setTimeout(() => {
        if (arrCell[goal].classList.contains('active')) {
          arrCell[goal].classList.remove('cellCollor');
          arrCell[goal].classList.add('active');
          this.pointUser++;
          console.log(this.pointUser);
        } else if (!arrCell[goal].classList.contains('active')) {
          arrCell[goal].classList.remove('cellCollor');
          arrCell[goal].classList.add('dissable');
          this.pointComputer++;
        }
        this.processTimer = null;
        this.startGame();
      }, this.timeInterval);
    }
  }
}

const newGame = new Game();
newGame.startGame();
