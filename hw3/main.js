class Employer{
  constructor(name , age , salary) {
    this.name = name
    this.age = age
    this.salary = salary
  }


  get nameInfo() {
    return this.name
  }

  set nameInfo(name) {
    return this.name
  }


  get ageInfo() {
    return this.age
  }

  set ageInfo(age) {
    return this.age
  }



  get salaryInfo() {
    return this.salary
  }

  set salaryInfo(salary) {
    return this.salary
  }
}



class Programmer extends Employer{
  constructor(name , age , salary , lang) {
    super(name , age , salary);
    this.lang = lang
  }

  get seleryInfo(){
    return this.salery*3
  }
}



const programmerOne = new Programmer("Roma" , 23 , 2000 ,{
  languageOne: "English",
  languageTwo: "Ukrainian",
  languageThree: "Franch"
})

console.log(programmerOne)


const programmerTwo = new Programmer("Danil" , 26 , 2800 ,{
  languageOne: "English",
  languageTwo: "Polish",
  languageThree: "Russian"
})

console.log(programmerTwo)

const programmerTree = new Programmer("Yulia" , 28 , 1800 ,{
  languageOne: "English",
  languageTwo: "Russian",
  languageThree: "Franch"
})

console.log(programmerTree)