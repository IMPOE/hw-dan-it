(function getElementProp() {
  const list = document.createElement('ul');
  document.body.append(list);
  (async () => {
    const res = await fetch('https://ajax.test-danit.com/api/swapi/films/');
    const data = await res.json();
    await data.map((item) => {
      const film = document.createElement('li');
      let characterList = '';
      (async () => {
        for (let i = 0; i < item.characters.length; i++) {
          const res = await fetch(item.characters[i]);
          const data = await res.json();
          const n = await data.name;
          characterList += ` ${n}`;
          if (i !== item.characters.length - 1) {
            characterList += ` ,`;
          }
        }
        function preLoader() {
          let load = document.createElement('div');
          load.id = 'load';
          film.appendChild(load);
          load.className = 'preloader';
          load.innerHTML =
            '<b>L</b><b>O</b><b>A</b><b>D</b><b>I</b><b>N</b><b>G</b>';
        }

        preLoader();

        const leter = function (ms) {
          return (loadImage = new Promise((resolve) => {
            let paragrafChar = document.createElement('p');
            paragrafChar.textContent = `People :${characterList}`;
            paragrafChar.style.fontWeight = 'bold';
            setTimeout(() => resolve(paragrafChar), ms);
          }));
        };
        leter(5000);

        loadImage.then((value) => {
          let load = document.getElementById('load');
          film.appendChild(value);
          load.remove();
        });
      })();

      let episopId = document.createElement('p');
      episodName = document.createElement('p');
      episodCrawl = document.createElement('p');
      episopId.textContent = `Episode: ${item.episodeId}`;
      episopId.style.fontWeight = 'bold';
      episodName.textContent = `Title: ${item.name}`;
      episodCrawl.textContent = `Opening_crawl: ${item.openingCrawl}`;
      film.append(episopId, episodName, episodCrawl);
      list.appendChild(film);
    });
  })();
})();

let a = 10 
b = 10 
c = a  + b 