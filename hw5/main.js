function findIP() {
  function createElDom(
    tagName = 'button',
    className = 'btn',
    domName = 'btnGetIp',
    textContent = ''
  ) {
    domName = document.createElement(tagName);
    domName.textContent = textContent;
    domName.className = className;
    document.body.append(domName);
  }

  async function getIP(url) {
    const res = await fetch(url);
    const data = await res.json();
    const ip = data.ip;
    return ip;
  }

  function elListener(el, e, url) {
    const currentEl = document.querySelector('.btn');
    currentEl.addEventListener(e, () => {
      getAdress(url);
    });
  }

  async function getAdress(url) {
    const ip = await getIP(url);
    const res = await fetch(
      `http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`
    );
    const data = await res.json();
    const adressIp = document.querySelector('.list');
    for (const [key, value] of Object.entries(data)) {
      let listItem = document.createElement('li');
      listItem.textContent = value;
      adressIp.appendChild(listItem);
      if (value === '') {
        listItem.textContent = 'None';
      }
    }
  }

  createElDom('ul', 'list', 'adressIp');
  createElDom('button', 'btn', 'btnGetIP', 'Вычислить по IP');
  elListener('btnGetIP', 'click', 'https://api.ipify.org/?format=json');
}
findIP();
